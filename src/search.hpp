#include <iostream>
#include <string>
#include <vector>
using namespace std;

int search(string text, string pattern)
{
	for (int i = 0; i < text.size(); i++)
	{
		for (int j = 0; j < pattern.size(); j++)
		{
			if (text[i] == pattern[j])
			return i;
		} 
	}
	return -1;
}
